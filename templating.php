<?php

/* GNU Local

   Copyright (C) 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once('config.php');
require_once('auth.php');
require_once('smarty/Smarty.class.php');

$smarty = new Smarty();

$smarty->template_dir = $install_path . '/themes/' . $theme . '/templates/';
$smarty->compile_dir = $install_path . '/themes/' . $theme . '/templates_c/';

$smarty->assign('base_url', $base_url);
$smarty->assign('theme', $theme);
$smarty->assign('img_url', $base_url . '/themes/' . $theme . '/img/');

if (isset($logged_in)) {
	$smarty->assign('logged_in', true);
	// Pre-fix this user's details with 'this_' to avoid confusion with other users
	$smarty->assign('this_user', $this_user);
}

header('Content-Type: text/html; charset=utf-8');
