<?php

/* GNU Local

   Copyright (C) 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


require_once('templating.php');

if(isset($_POST['checkin']) && !empty($_POST['latitude']) && !empty($_POST['longitude'])) {
	$lat = (double) $_POST['latitude'];
	$long = (double) $_POST['longitude'];
	$locations = Server::getLocationsInArea($lat, $long, 100);
	if(empty($locations)) {
		$smarty->assign('latitude', $lat);
		$smarty->assign('longitude', $long);
		$smarty->assign('new_location', true);
		$smarty->assign('location_types', Server::getLocationTypes());
	} else {
		if($locations[0]['distance'] < 10) {
			// If they're within 10 meters of a location assume it's the right one and check them in
			$adodb->Execute('INSERT INTO Check_Ins (userid, location) VALUES ('
				. $this_user->id . ', '
				. $locations[0]['location']->id . ')');

			$smarty->assign('checked_in', true);
			$smarty->assign('place_name', $locations[0]['location']->name);
		} else {
			// Display a list of possible nearby locations or give the option to create a new one
			$smarty->assign('locations', $locations);
		}
	}
}

if(isset($_POST['submit'])) {
	$errors = array();
	if(empty($_POST['name'])) {
		$errors[] = 'You must give a name for this location.';
	}
	if(empty($_POST['type'])) {
		$errors[] = 'You must select a type for this location.';
	}
	if(empty($_POST['latitude']) || empty($_POST['longitude'])) {
		$errors[] = 'You must give co-ordinates for this location.';
	}

	if(empty($errors)) {
		$adodb->Execute('INSERT INTO Locations (name, type, latitude, longitude, description) VALUES ('
			. $adodb->qstr($_POST['name']) . ', '
			. ((int) $_POST['type']) . ', '
			. ((double) $_POST['latitude']) . ', '
			. ((double) $_POST['longitude']) . ', '
			. $adodb->qstr($_POST['description']) . ')');

		$row = $adodb->GetOne('SELECT id FROM Locations WHERE name=' . $adodb->qstr($_POST['name'])
			. ' AND latitude = ' . ((double) $_POST['latitude'])
			. ' AND longitude = ' . ((double) $_POST['longitude']));

		$adodb->Execute('INSERT INTO Check_Ins (userid, location) VALUES ('
			. $this_user->id . ', '
			. $row['id'] . ')');

		$smarty->assign('checked_in', true);
		$smarty->assign('place_name', $_POST['name']);
	} else {
		$smarty->assign('errors', $errors);
	}
}

$smarty->display('welcome.tpl');
