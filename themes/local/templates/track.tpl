{if $new_location}
	<h2>Looks like you've found somewhere new!</h2>
	<p>Tell us a little about the place you're in:</p>
	<form method='post' action=''>
		<input type='hidden' name='latitude' value='{$latitude}' />
		<input type='hidden' name='longitude' value='{$longitude}' />
		<label for='name'>Name: </label><input type='text' name='name' /><br />
		<label for='type'>Type of place: </label><select name='type'>
		{foreach from=$location_types item=ltype}
			<option value='{$ltype.id}'>{$ltype.name}</option>
		{/foreach}
		</select><br />
		<label for='description'>Description: </label><br />
		<textarea name='description'></textarea><br /><br />
		<input type='submit' value='Submit' name='submit' />
	</form>
{elseif $locations}

{else}

{if $checked_in}
	<p>You've just checked in to {$place_name}!</p>
{/if}

<script>
{literal}
var gl;

function displayPosition(position) {
	var p = document.getElementById("p");
	var i = position.coords.latitude + ',' + position.coords.longitude + '&zoom=17&size=300x300&markers=' + position.coords.latitude + ',' + position.coords.longitude + ',ltblu-pushpin';
	
	var h = "<img src=http://staticmap.openstreetmap.de/staticmap.php?center=" + i + "><br /><br />";
	{/literal}{if !$checked_in}{literal}
	h += "<form method='post' action=''>";
	h += "<input type='hidden' name='latitude' id='latitude' value='' />";
	h += "<input type='hidden' name='longitude' id='longitude' value='' />";
	h += "<input type='submit' name='checkin' value='Check in' />";
	h += "</form>";
	{/literal}{/if}{literal}
	p.innerHTML = h;

	var lat = document.getElementById("latitude");
	var lon = document.getElementById("longitude");
	lat.value = position.coords.latitude;
	lon.value = position.coords.longitude;
}

function displayError(positionError) {
	alert("error");
}

try {
	if (typeof navigator.geolocation === 'undefined'){
		gl = google.gears.factory.create('beta.geolocation');
	} else {
		gl = navigator.geolocation;
	}
} catch(e) {}

if (gl) {
	gl.getCurrentPosition(displayPosition, displayError);
} else {
	alert("Geolocation services are not supported by your web browser.");
}
{/literal}
</script>

<div id="p" style="width: 300px; height: 300px; margin: 0 auto; border: 1px solid black; background-image:url('spinner.gif'); background-repeat: no-repeat; background-position: center; text-align: center;">Loading map image...</div>
{/if}
