<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
<Folder>
<name>local checkins</name>
<description>Blah</description>
{foreach from=$locations item=foo}
<Placemark>
<name>{$foo.name}</name>
<description>{$foo.description}</description>
<visibility>1</visibility>
<Point>
<coordinates>
  {$foo.longitude},{$foo.latitude}
</coordinates>
</Point>
</Placemark>
{/foreach}
</Folder>
</kml>
