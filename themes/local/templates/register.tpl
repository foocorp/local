{include file='header.tpl'}

{if isset($activated)}

        <h2>You're in!</h2>

	<p>Your account has been activated! You may now <a href="/login.php">login!</a></p>

{elseif isset($registered)}

	<h2>Go! Go! Go! Check your email now</h2>

	<p>Please follow the link in your email to activate your account!</p>
	
{else}

<h2>You look awesome today, by the way</h2>

	{if isset($errors)}
		<p id='errors'>{$errors}</p>
	{/if}

	<form action='' method='post'>
		<fieldset>

			<h3><label for='username'>Your username:</label></h3>
			<p><small>No more than 16 characters, please.</small></p>
			<div><input id='username' name='username' type='text' value='{$username}' maxlength='16' size='16' /></div>

			<h3>
			<label for='password'>Your password:</label></h3>
			<p><small>Try and make it hard to guess! Don't use the same password for everything!</small></p>
			<div><input id='password' name='password' type='password' value=''/></div>

			<h3>
			<label for='password-repeat'>Your password again</label></h3>
			<p><small>Who said repeating yourself was a bad thing?</small></p>
			<div><input id='password-repeat' name='password-repeat' type='password' value=''/></div>

			<h3><label for='email'>Your e-mail:</label></h3>
			<p><small>We're going to email you here to confirm this account.</small></p>
			<div><input id='email' name='email' type='text' value='{$email}' maxlength='64' /></div>

			<p><label><input type="checkbox" name="foo-check" /> I read this form carefully, and double-checked my email address first, honest.</label></p>

		</fieldset>

		<p><input type='submit' name='register' value="Sign up" /></p>

	</form>

	<p><small>We won't sell, swap or give away your email address. You can optionally include personal data on your profile, which is displayed publicly.</small></p>
	
{/if}

{include file='footer.tpl'}
