<!DOCTYPE html>
<html>
<head>
<meta content="width=320" name="viewport" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	{if !($pagetitle)}
		<title>local</title>
	{else}
		<title>{$pagetitle|escape:'html':'UTF-8'} &mdash; local</title>
	{/if}
	<link rel="stylesheet" href="{$base_url}/themes/{$theme}/media/style.css" type="text/css" />
</head>

<body>
	<h1><a href="{$base_url}">local</a></h1>

<div id="content">
