{include file='header.tpl'}

<h2>Login</h2>

<h3>Need an account? <a href="{$base_url}/register.php">Register now!</a></h3>

{if isset($errors)}
        <p id='errors'>{$errors}</p>
{/if}

<form method="post">
{include file='login-form.tpl'}
</form>

{include file='footer.tpl'}
