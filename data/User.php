<?php

/* GNU Local

   Copyright (C) 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once($install_path . '/database.php');
require_once($install_path . '/data/Server.php');

/**
 * Represents User data
 *
 * General attributes are accessible as public variables.
 *
 */
class User {

	public $name, $email, $fullname, $bio, $homepage, $error, $userlevel;
	public $id, $avatar_uri;
	public $password, $created, $modified, $receive_emails;

	/**
	 * User constructor
	 *
	 * @param string $name The name of the user to load
	 */
	function __construct($name, $data = null) {

		global $base_url;
		$base = preg_replace('#/$#', '', $base_url);

		if (is_array($data)) {
			$row = $data;
		} else {
			global $adodb;
			$query = 'SELECT * FROM Users WHERE lower(username) = lower(' . $adodb->qstr($name) . ') LIMIT 1';
			$adodb->SetFetchMode(ADODB_FETCH_ASSOC);
			$row = $adodb->CacheGetRow(7200, $query);
			if (!$row) {
				throw new Exception('EUSER', 22);
			}
		}

		if (is_array($row)) {
			$this->name             = $row['username'];
			$this->password         = $row['password'];
			$this->email            = $row['email'];
			$this->fullname         = $row['fullname'];
			$this->homepage         = $row['homepage'];
			$this->bio              = $row['bio'];
			$this->userlevel        = $row['userlevel'];
			$this->id               = $row['id'];
			$this->avatar_uri       = $row['avatar_uri'];
			$this->created          = $row['created'];
			$this->modified         = $row['modified'];
			$this->receive_emails   = $row['receive_emails'];
		}
	}

	public static function new_from_uniqueid_number($uid) {
		global $adodb;
		$query = sprintf('SELECT * FROM Users WHERE id = %d', (int)$uid);
		$adodb->SetFetchMode(ADODB_FETCH_ASSOC);
		$row = $adodb->CacheGetRow(7200, $query);

		if ($row) {
			try {
				return new User($row['username'], $row);
			} catch (Exception $e) {
				return false;
			}
		} else {
			return false;
		}
	}

	function save() {
		global $adodb;

		$q = sprintf('UPDATE Users SET '
				. 'email=%s, '
				. 'password=%s, '
				. 'fullname=%s, '
				. 'homepage=%s, '
				. 'bio=%s, '
				. 'userlevel=%d, '
				. 'avatar_uri=%s, '
				. 'receive_emails=%d, '
				. 'modified=NOW() '
				. 'WHERE id=%d'
				, $adodb->qstr($this->email)
				, $adodb->qstr($this->password)
				, $adodb->qstr($this->fullname)
				, $adodb->qstr($this->homepage)
				, $adodb->qstr($this->bio)
				, $adodb->qstr($this->location)
				, $this->userlevel
				, $adodb->qstr($this->avatar_uri)
				, (int)($this->receive_emails)
				, (int)($this->id));

		try {
			$res = $adodb->Execute($q);
		} catch (Exception $e) {
			header('Content-Type: text/plain');
			exit;
		}

		$query = 'SELECT * FROM Users WHERE lower(username) = lower(' . $adodb->qstr($this->name) . ') LIMIT 1';
		$adodb->CacheFlush($query);

		return 1;
	}

	/**
	 * Retrieve a user's avatar via the gravatar service
	 *
	 * @param int $size The desired size of the avatar (between 1 and 512 pixels)
	 * @return array A URL to the user's avatar image
	 */
	function getAvatar($size = 64) {
		if (!empty($this->avatar_uri)) {
			return $this->avatar_uri;
		}

		return 'http://www.gravatar.com/avatar/' . md5(strtolower($this->email)) . '?s=' . $size . '&d=monsterid';
	}

	function getURL($component = 'profile') {
		return Server::getUserURL($this->name, $component);
	}

}
