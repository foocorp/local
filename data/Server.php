<?php

/* GNU Local 
 
   Copyright (C) 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once($install_path . '/database.php');
require_once($install_path . '/data/User.php');
require_once($install_path . '/data/Location.php');
require_once($install_path . '/utils/rewrite-encode.php');

/**
 * Provides access to server-wide data
 *
 * All methods are statically accessible
 */
class Server {


	/**
	 * The get*URL functions are implemented here rather than in their respective
	 * objects so that we can produce URLs without needing to build whole objects.
	 *
	 * @param string $username The username we want a URL for
	 * @return A string containing URL to the user's profile
	 */
	static function getUserURL ($username, $component = 'profile') {
		global $friendly_urls, $base_url;
		if ($component == 'edit') {
			return $base_url . '/user-edit.php';
		} else if ($friendly_urls) {
			if ($component == 'profile') {
				$component = '';
			} else {
				$component = "/{$component}";
			}
			return $base_url . '/user/' . rewrite_encode($username) . $component;
		} else {
			return $base_url . "/user-{$component}.php?user=" . urlencode($username);
		}
	}

	static function getGroupURL($groupname) {
		global $friendly_urls, $base_url;
		if ($friendly_urls) {
			return $base_url . '/group/' . rewrite_encode($groupname);
		} else {
			return $base_url . '/group.php?group=' . urlencode($groupname);
		}
	}

	static function getLocationURL($id) {
		global $friendly_urls, $base_url;
		if ($friendly_urls) {
			return $base_url . '/location/' . $id;
		} else {
			return $base_url . '/show-location.php?id=' . $id;
		}
	}

	/**
	 * Finds a list of locations within a given radius of
	 * the provided co-ordinates.
	 *
	 * @param double $lat Latitude in decimal degrees
	 * @param double $long Longitude in decimal degrees
	 * @param double $radius Radius in meters (maximum radius ~20km)
	 * @return An array of Location objects and their distances
	 */
	static function getLocationsInArea($lat, $long, $radius) {
		global $adodb;
		$lat = (double) $lat;
		$long = (double) $long;
		$radius = (double) $radius;

		$res = $adodb->GetAll('SELECT id, distance FROM '
			. '(SELECT id, acos(sin(radians(' . $lat . ')) * sin(radians(latitude)) '
			. '+ cos(radians(' . $lat . ')) * cos(radians(latitude)) '
			. '* cos(radians(longitude) - radians(' . $long . '))) * 6371000 AS distance FROM '
			. '(SELECT id, latitude, longitude FROM Locations WHERE '
			. 'latitude > ' . ($lat - 0.1) . ' AND latitude < ' . ($lat + 0.1) . ' AND '
			. 'longitude > ' . ($long - 0.1) . ' AND longitude < ' . ($long + 0.1)
			. ') AS RoughEstimate) AS AccurateLocations WHERE distance < ' . $radius);

		$locations = array();
		foreach($res as &$row) {
			$locations[] = array('location' => new Location($row['id']), 'distance' => $row['distance']);
		}

		return $locations;
	}

	/**
	 * Retrieves a list of location types.
	 *
	 * @return An array of location type names and their ids
	 */
	static function getLocationTypes() {
		global $adodb;

		$res = $adodb->CacheGetAll(600, 'SELECT * FROM Location_Types');
		return $res;
	}

	static function search($search_term, $search_type, $limit = 40) {
		global $adodb;
		switch ($search_type) {
			case 'user':
				$table = 'Users';
				$search_fields[] = 'username';
				$search_fields[] = 'fullname';
				$data_fields[] = 'username';
				$data_fields[] = 'fullname';
				$data_fields[] = 'bio';
				break;
			case 'location':
				$table = 'Locations';
				$search_fields[] = 'name';
				$data_fields[] = 'id';
				$data_fields[] = 'name';
				break;
			default:
				return array();
		}

		$sql = 'SELECT DISTINCT ';

		for ($i = 0; $i < count($data_fields); $i++) {
			$sql .= $data_fields[$i];
			if ($i < count($data_fields) - 1) {
				$sql .= ', ';
			}
		}

		$sql .= ' FROM ' . $table . ' WHERE ';

		for ($i = 0; $i < count($search_fields); $i++) {
			if ($i > 0) {
				$sql .= ' OR ';
			}
			$sql .= 'LOWER(' . $search_fields[$i] . ') LIKE LOWER(' . $adodb->qstr('%' . $search_term . '%') . ')';
		}

		$sql .= 'LIMIT ' . $limit;

		$res = $adodb->CacheGetAll(600, $sql);

		$result = array();
		foreach ($res as &$i) {
			$row = sanitize($i);
			switch ($search_type) {
				case 'user':
					$row['url'] = Server::getUserURL($row['username']);
					break;
				case 'location':
					$row['url'] = Server::getLocationURL($row['id']);
					break;
			}
			$result[] = $row;
		}

		return $result;
	}

}
