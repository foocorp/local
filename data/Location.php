<?php

/* GNU Local

   Copyright (C) 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once($install_path . '/database.php');
require_once($install_path . '/data/Server.php');

/**
 * Represents Location data
 *
 * General attributes are accessible as public variables.
 *
 */
class Location {

	public $id, $latitude, $longitude, $type, $name, $description;
	private $query;

	/**
	 * Location constructor
	 *
	 * @param int $id The id of the location to load
	 */
	function __construct($id) {
		global $adodb;

		$this->query = 'SELECT Locations.id AS id, Locations.name AS name, type, Location_Types.name AS type_name, Locations.description, latitude, longitude FROM Locations INNER JOIN Location_Types ON type = Location_Types.id WHERE Locations.id = ' . ((int) $id);
		$row = $adodb->CacheGetRow(7200, $this->query);
		if (!$row) {
			throw new Exception('ELOCATION', 22);
		}

		$this->id               = $row['id'];
		$this->name             = $row['name'];
		$this->type_id          = $row['type'];
		$this->type_name        = $row['type_name'];
		$this->description      = $row['description'];
		$this->latitude         = $row['latitude'];
		$this->longitude        = $row['longitude'];
	}

	function save() {
		global $adodb;

		$q = sprintf('UPDATE Locations SET '
				. 'name=%s, '
				. 'description=%s, '
				. 'type=%d, '
				. 'latitude=%f, '
				. 'longitude=%f, '
				. 'WHERE id=%d'
				, $adodb->qstr($this->name)
				, $adodb->qstr($this->description)
				, ((int)$this->type_id)
				, $this->longitude
				, $this->latitude
				, ((int)$this->id));

		try {
			$res = $adodb->Execute($q);
		} catch (Exception $e) {
			header('Content-Type: text/plain');
			exit;
		}

		$adodb->CacheFlush($this->query);
	}

	function getURL($component = 'profile') {
		return Server::getLocationURL($this->id);
	}

}
