<?php

/* GNU Local

   Copyright (C) 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once('adodb/adodb-exceptions.inc.php');
require_once('adodb/adodb.inc.php');
require_once('version.php');
require_once('utils/get_absolute_url.php');

if (file_exists('config.php')) {
	die('A configuration file already exists. Please delete <i>config.php</i> if you wish to reinstall.');
}

if (isset($_POST['install'])) {

	//Get the database connection string
	$dbms = $_POST['dbms'];
	if(empty($_POST['password'])) {
		$connect_string = $dbms . '://dbname=' . $_POST['dbname'];
	} else {
		$connect_string = $dbms . '://' . $_POST['username'] . ':' . $_POST['password'] . '@' . $_POST['hostname'] . ':' . $_POST['port'] . '/' . $_POST['dbname'];
	}

	try {
		$adodb =& NewADOConnection($connect_string);
	} catch (Exception $e) {
		var_dump($e);
		adodb_backtrace($e->gettrace());
		die("Database connection failure\n");
	}

	// Create tables

	$queries = array(
		'CREATE TABLE Location_Types (
		id SERIAL PRIMARY KEY,
		name TEXT,
		description TEXT)',

		'CREATE TABLE Locations (
		id SERIAL PRIMARY KEY,
		name TEXT,
		type INTEGER REFERENCES Location_Types(id),
		latitude FLOAT,
		longitude FLOAT,
		description TEXT,
		UNIQUE(name, latitude, longitude))',

		'INSERT INTO Location_Types (name) VALUES (\'Airport\')',
		'INSERT INTO Location_Types (name) VALUES (\'Amusement Park\')',
		'INSERT INTO Location_Types (name) VALUES (\'Art Gallery\')',
		'INSERT INTO Location_Types (name) VALUES (\'Bed and Breakfast\')',
		'INSERT INTO Location_Types (name) VALUES (\'Brewery\')',
		'INSERT INTO Location_Types (name) VALUES (\'Bus Stop\')',
		'INSERT INTO Location_Types (name) VALUES (\'Cafe\')',
		'INSERT INTO Location_Types (name) VALUES (\'Camp Site\')',
		'INSERT INTO Location_Types (name) VALUES (\'Castle\')',
		'INSERT INTO Location_Types (name) VALUES (\'Cathedral\')',
		'INSERT INTO Location_Types (name) VALUES (\'Cave\')',
		'INSERT INTO Location_Types (name) VALUES (\'Cemetry\')',
		'INSERT INTO Location_Types (name) VALUES (\'Chapel\')',
		'INSERT INTO Location_Types (name) VALUES (\'Church\')',
		'INSERT INTO Location_Types (name) VALUES (\'Cinema\')',
		'INSERT INTO Location_Types (name) VALUES (\'Concert Hall\')',
		'INSERT INTO Location_Types (name) VALUES (\'Courthouse\')',
		'INSERT INTO Location_Types (name) VALUES (\'Dam\')',
		'INSERT INTO Location_Types (name) VALUES (\'Distillary\')',
		'INSERT INTO Location_Types (name) VALUES (\'Exhibition Centre\')',
		'INSERT INTO Location_Types (name) VALUES (\'Factory\')',
		'INSERT INTO Location_Types (name) VALUES (\'Farm\')',
		'INSERT INTO Location_Types (name) VALUES (\'Fleemarket\')',
		'INSERT INTO Location_Types (name) VALUES (\'Forest\')',
		'INSERT INTO Location_Types (name) VALUES (\'Fountain\')',
		'INSERT INTO Location_Types (name) VALUES (\'Garage\')',
		'INSERT INTO Location_Types (name) VALUES (\'Golf Course\')',
		'INSERT INTO Location_Types (name) VALUES (\'Harbour\')',
		'INSERT INTO Location_Types (name) VALUES (\'Hill\')',
		'INSERT INTO Location_Types (name) VALUES (\'Historical Building\')',
		'INSERT INTO Location_Types (name) VALUES (\'Hospital\')',
		'INSERT INTO Location_Types (name) VALUES (\'Hotel\')',
		'INSERT INTO Location_Types (name) VALUES (\'House\')',
		'INSERT INTO Location_Types (name) VALUES (\'Ice Skating Rink\')',
		'INSERT INTO Location_Types (name) VALUES (\'Island\')',
		'INSERT INTO Location_Types (name) VALUES (\'Lake\')',
		'INSERT INTO Location_Types (name) VALUES (\'Landmark\')',
		'INSERT INTO Location_Types (name) VALUES (\'Leisure Centre\')',
		'INSERT INTO Location_Types (name) VALUES (\'Library\')',
		'INSERT INTO Location_Types (name) VALUES (\'Lighthouse\')',
		'INSERT INTO Location_Types (name) VALUES (\'Market\')',
		'INSERT INTO Location_Types (name) VALUES (\'Metro Station\')',
		'INSERT INTO Location_Types (name) VALUES (\'Monastery\')',
		'INSERT INTO Location_Types (name) VALUES (\'Monument\')',
		'INSERT INTO Location_Types (name) VALUES (\'Mosque\')',
		'INSERT INTO Location_Types (name) VALUES (\'Motel\')',
		'INSERT INTO Location_Types (name) VALUES (\'Mountain Pass\')',
		'INSERT INTO Location_Types (name) VALUES (\'Mountain Peak\')',
		'INSERT INTO Location_Types (name) VALUES (\'Museum\')',
		'INSERT INTO Location_Types (name) VALUES (\'Night Club\')',
		'INSERT INTO Location_Types (name) VALUES (\'Observatory\')',
		'INSERT INTO Location_Types (name) VALUES (\'Opera\')',
		'INSERT INTO Location_Types (name) VALUES (\'Palace\')',
		'INSERT INTO Location_Types (name) VALUES (\'Park\')',
		'INSERT INTO Location_Types (name) VALUES (\'Police Station\')',
		'INSERT INTO Location_Types (name) VALUES (\'Post Office\')',
		'INSERT INTO Location_Types (name) VALUES (\'Pub\')',
		'INSERT INTO Location_Types (name) VALUES (\'Quarry\')',
		'INSERT INTO Location_Types (name) VALUES (\'Racetrack\')',
		'INSERT INTO Location_Types (name) VALUES (\'Restruant\')',
		'INSERT INTO Location_Types (name) VALUES (\'Ruin\')',
		'INSERT INTO Location_Types (name) VALUES (\'Sauna\')',
		'INSERT INTO Location_Types (name) VALUES (\'Shop\')',
		'INSERT INTO Location_Types (name) VALUES (\'Shopping Centre\')',
		'INSERT INTO Location_Types (name) VALUES (\'Spa\')',
		'INSERT INTO Location_Types (name) VALUES (\'Synagoge\')',
		'INSERT INTO Location_Types (name) VALUES (\'Temple\')',
		'INSERT INTO Location_Types (name) VALUES (\'Theatre\')',
		'INSERT INTO Location_Types (name) VALUES (\'Toilet\')',
		'INSERT INTO Location_Types (name) VALUES (\'Tower\')',
		'INSERT INTO Location_Types (name) VALUES (\'Town Hall\')',
		'INSERT INTO Location_Types (name) VALUES (\'Train Station\')',
		'INSERT INTO Location_Types (name) VALUES (\'Tram Station\')',
		'INSERT INTO Location_Types (name) VALUES (\'Tunnel\')',
		'INSERT INTO Location_Types (name) VALUES (\'University\')',
		'INSERT INTO Location_Types (name) VALUES (\'View Point\')',
		'INSERT INTO Location_Types (name) VALUES (\'Vulcano\')',
		'INSERT INTO Location_Types (name) VALUES (\'Weir\')',
		'INSERT INTO Location_Types (name) VALUES (\'Well\')',
		'INSERT INTO Location_Types (name) VALUES (\'Windmill\')',
		'INSERT INTO Location_Types (name) VALUES (\'Woodland\')',
		'INSERT INTO Location_Types (name) VALUES (\'Youth Hostel\')',
		'INSERT INTO Location_Types (name) VALUES (\'Zoo\')',

		'CREATE TABLE Users (
		id SERIAL PRIMARY KEY,
		username VARCHAR(64) unique,
		password VARCHAR(32) NOT NULL,
		email VARCHAR(255),
		fullname VARCHAR(255),
		bio TEXT,
		homepage VARCHAR(255),
		location VARCHAR(255),
		userlevel INTEGER DEFAULT 0,
		avatar_uri VARCHAR(255),
		openid_uri VARCHAR(100),
		active INTEGER DEFAULT 0,
		created TIMESTAMP DEFAULT NOW(),
		modified TIMESTAMP DEFAULT NOW(),
		receive_emails INTEGER DEFAULT 1)',

		'CREATE TABLE Account_Activation (
		userid INTEGER REFERENCES Users(id),
		authcode VARCHAR(32))',

		'CREATE TABLE Check_Ins (
		userid INTEGER REFERENCES Users(id),
		location INTEGER REFERENCES Locations(id),
		time TIMESTAMP DEFAULT NOW())',

		'CREATE TABLE Friends (
		userid1 INTEGER REFERENCES Users(id),
		userid2 INTEGER REFERENCES Users(id),
		authorised INTEGER DEFAULT 0)'
	);

	foreach ($queries as $query) {
		try {
			$adodb->Execute($query);
		} catch (Exception $e) {
			die('Database Error: ' . $adodb->ErrorMsg());
		}
	}

	$adodb->Close();

	$install_path = dirname(__FILE__) . '/';
	$theme = $_POST['theme'];
	$base_url = $_POST['base_url'];

	if ($base_url[strlen($base_url) - 1] === '/') {
		$base_url = substr($base_url, 0, -1);
	}

	//Write out the configuration
	$config = "<?php\n \$config_version = " . $version .";\n \$theme = '" . $theme . "';\n \$base_url = '" . $base_url . "';\n \$connect_string = '" . $connect_string . "';\n \$submissions_server = '" . $submissions_server . "';\n \$install_path = '" . $install_path . "';\n \$adodb_connect_string = '" . $connect_string . "'; ";

	$conf_file = fopen('config.php', 'w');
	$result = fwrite($conf_file, $config);
	fclose($conf_file);

	if (!$result) {
		$print_config = str_replace('<', '&lt;', $config);
		die('Unable to write to file \'<i>config.php</i>\'. Please create this file and copy the following in to it: <br /><pre>' . $print_config . '</pre>');
	}

	die('Configuration completed successfully!');
}

?>
<html>
	<head>
		<title>Local Installer</title>
	</head>

	<body>
		<h1>Local Installer</h1>
		<form method="post">
			<h2>Database</h2>
			Database Management System: <br />
			<input type="radio" name="dbms" value="mysql" checked>MySQL</input><br />
			<input type="radio" name="dbms" value="pgsql">PostgreSQL</input><br />
			<br />
			Hostname: <input type="text" name="hostname" /><br />
			Port: <input type="text" name="port" /><br />
			Database: <input type="text" name="dbname" /><br />
			Username: <input type="text" name="username" /><br />
			Password: <input type="password" name="password" /><br />
			<br />
			<h2>General</h2>
			Theme: <select name="theme">
			<?php  
				$dir = opendir('themes');
				while ($theme = readdir($dir)) {
					if (is_dir('themes/' . $theme) && $theme[0] != '.') {
						echo '<option>' . $theme . '</option>';
					}
				}
			?>
			</select><br />
			Base URL: <input type="text" name="base_url" value="<?php echo getAbsoluteURL(); ?>" /><br />
			<input type="submit" value="Install" name="install" />
		</form>
	</body>
</html>


